package com.kohutyan.ezlo_test.core.di.modules

import android.content.Context
import com.chuckerteam.chucker.api.ChuckerInterceptor
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.kohutyan.ezlo_test.BuildConfig
import com.kohutyan.ezlo_test.data.network.DateDeserializer
import com.kohutyan.ezlo_test.utils.DATE_FORMAT
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.Date
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class NetworkModule {

    @Singleton
    @Provides
    fun createRetrofit(
        okHttpClient: OkHttpClient,
        gson: Gson,
    ): Retrofit = Retrofit.Builder().baseUrl(BuildConfig.BASE_URL)
        .addConverterFactory(ScalarsConverterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson)).client(okHttpClient).build()

    @Provides
    internal fun okHttpClient(
        chuck: ChuckerInterceptor,
    ): OkHttpClient {
        val builder = OkHttpClient.Builder().addInterceptor(chuck)

        return builder.build()
    }

    @Provides
    internal fun createGson() = GsonBuilder().setDateFormat(DATE_FORMAT)
        .registerTypeAdapter(Date::class.java, DateDeserializer()).create()

    @Provides
    internal fun chuckInterceptor(@ApplicationContext context: Context): ChuckerInterceptor =
        ChuckerInterceptor.Builder(context).build()
}