package com.kohutyan.ezlo_test.core.di.modules

import com.kohutyan.ezlo_test.data.db.dao.DeviceDao
import com.kohutyan.ezlo_test.data.network.service.DeviceService
import com.kohutyan.ezlo_test.data.repo.DeviceRepo
import com.kohutyan.ezlo_test.domain.repo.IDeviceRepo
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepoModule {
    @Provides
    @Singleton
    fun provideDeviceRepo(
        retrofit: Retrofit,
        dao: DeviceDao
    ): IDeviceRepo = DeviceRepo(retrofit.create(DeviceService::class.java), dao)
}