package com.kohutyan.ezlo_test.core.domain.use_cases

abstract class UseCase<out Type, in Params> where Type : Any? {

    abstract fun run(params: Params): Type
}