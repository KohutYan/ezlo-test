package com.kohutyan.ezlo_test.core.di.modules

import android.content.Context
import androidx.room.Room
import com.kohutyan.ezlo_test.data.db.AppDatabase
import com.kohutyan.ezlo_test.data.db.dao.DeviceDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DatabaseModule {
    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): AppDatabase = Room.databaseBuilder(
        context, AppDatabase::class.java, AppDatabase.DB_NAME
    ).build()

    @Provides
    @Singleton
    fun provideDeviceDao(database: AppDatabase): DeviceDao = database.deviceDao()
}