package com.kohutyan.ezlo_test.core.domain.use_cases

abstract class CoroutineUseCase<out Type, in Params> where Type : Any? {

    abstract suspend fun run(params: Params): Type
}
