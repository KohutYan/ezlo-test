package com.kohutyan.ezlo_test.domain.use_cases

import com.kohutyan.ezlo_test.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.ezlo_test.domain.models.DeviceModel
import com.kohutyan.ezlo_test.domain.repo.IDeviceRepo
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ObserveDevicesFromDb @Inject constructor(
    private val repo: IDeviceRepo
) :
    CoroutineUseCase<Flow<List<DeviceModel>>, Unit>() {
    override suspend fun run(params: Unit): Flow<List<DeviceModel>> {
        return repo.watchDevicesFromDb()
    }
}