package com.kohutyan.ezlo_test.domain.repo

import com.kohutyan.ezlo_test.domain.models.DeviceModel
import kotlinx.coroutines.flow.Flow

interface IDeviceRepo {
    suspend fun getDevicesFromApi(): Flow<List<DeviceModel>>
    suspend fun watchDevicesFromDb(): Flow<List<DeviceModel>>
    suspend fun deleteDeviceById(id: Int)
    suspend fun updateDevice(device: DeviceModel)
}