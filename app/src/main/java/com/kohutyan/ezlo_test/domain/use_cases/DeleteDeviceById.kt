package com.kohutyan.ezlo_test.domain.use_cases

import com.kohutyan.ezlo_test.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.ezlo_test.domain.repo.IDeviceRepo
import javax.inject.Inject

class DeleteDeviceById @Inject constructor(
    private val repo: IDeviceRepo
) : CoroutineUseCase<Unit, Int>() {
    override suspend fun run(params: Int) {
        repo.deleteDeviceById(params)
    }
}