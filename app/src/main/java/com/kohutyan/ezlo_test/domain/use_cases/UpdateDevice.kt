package com.kohutyan.ezlo_test.domain.use_cases

import com.kohutyan.ezlo_test.core.domain.use_cases.CoroutineUseCase
import com.kohutyan.ezlo_test.domain.models.DeviceModel
import com.kohutyan.ezlo_test.domain.repo.IDeviceRepo
import javax.inject.Inject

class UpdateDevice @Inject constructor(
    private val repo: IDeviceRepo
) : CoroutineUseCase<Unit, DeviceModel>() {
    override suspend fun run(params: DeviceModel) {
        repo.updateDevice(params)
    }
}