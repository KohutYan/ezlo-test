package com.kohutyan.ezlo_test.domain.models

data class DeviceModel(
    val deviceName: String,
    val firmware: String,
    val internalIP: String,
    val lastAliveReported: String,
    val macAddress: String,
    val pKAccount: Int,
    val pKDevice: Int,
    val pKDeviceSubType: Int,
    val pKDeviceType: Int,
    val platform: String?,
    val serverAccount: String,
    val serverDevice: String,
    val serverEvent: String
)
