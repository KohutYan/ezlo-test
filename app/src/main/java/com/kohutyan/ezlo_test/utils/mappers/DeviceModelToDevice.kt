package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.domain.models.DeviceModel
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device

object DeviceModelToDevice : Mapper<DeviceModel, Device> {
    override fun map(input: DeviceModel): Device {
        return input.run {
            Device(
                deviceName,
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                PlatformMapper.map(platform),
                serverAccount,
                serverDevice,
                serverEvent
            )
        }
    }
}