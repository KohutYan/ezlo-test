package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.data.network.models.DeviceResponse
import com.kohutyan.ezlo_test.domain.models.DeviceModel

object DeviceResponseToModelMapper : Mapper<Pair<DeviceResponse, Int>, DeviceModel> {
    override fun map(input: Pair<DeviceResponse, Int>): DeviceModel {
        return input.first.run {
            DeviceModel(
                "House number ${input.second + 1}",
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                platform,
                serverAccount,
                serverDevice,
                serverEvent
            )
        }
    }
}