package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.db.entities.DeviceEntity
import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.domain.models.DeviceModel

object DeviceEntityToModelMapper : Mapper<DeviceEntity, DeviceModel> {
    override fun map(input: DeviceEntity): DeviceModel {
        return input.run {
            DeviceModel(
                deviceName,
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                platform,
                serverAccount,
                serverDevice,
                serverEvent,
            )
        };
    }
}