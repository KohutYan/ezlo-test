package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.domain.models.DeviceModel
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device

object DeviceToDeviceModelMapper : Mapper<Device, DeviceModel> {
    override fun map(input: Device): DeviceModel {
        return input.run {
            DeviceModel(
                deviceName,
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                platform.platformKey,
                serverAccount,
                serverDevice,
                serverEvent
            )
        }
    }
}