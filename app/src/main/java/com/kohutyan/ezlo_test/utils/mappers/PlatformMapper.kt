package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Platform

object PlatformMapper : Mapper<String?, Platform> {
    override fun map(input: String?): Platform {
        return input?.let {
            val platform =
                Platform.values().find { it.platformKey == input } ?: throw Exception("No element")

            platform
        } ?: Platform.NO_KEY
    }
}