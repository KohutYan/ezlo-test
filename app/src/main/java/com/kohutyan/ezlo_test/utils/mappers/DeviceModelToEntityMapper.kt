package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.db.entities.DeviceEntity
import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.domain.models.DeviceModel

object DeviceModelToEntityMapper : Mapper<DeviceModel, DeviceEntity> {
    override fun map(input: DeviceModel): DeviceEntity {
        return input.run {
            DeviceEntity(
                deviceName,
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                platform,
                serverAccount,
                serverDevice,
                serverEvent
            )
        }
    }
}