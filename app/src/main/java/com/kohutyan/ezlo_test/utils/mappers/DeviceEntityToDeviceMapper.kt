package com.kohutyan.ezlo_test.utils.mappers

import com.kohutyan.ezlo_test.data.db.entities.DeviceEntity
import com.kohutyan.ezlo_test.data.mappers.Mapper
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device

object DeviceEntityToDeviceMapper : Mapper<DeviceEntity, Device> {
    override fun map(input: DeviceEntity): Device {
        return input.run {
            Device(
                deviceName,
                firmware,
                internalIP,
                lastAliveReported,
                macAddress,
                pKAccount,
                pKDevice,
                pKDeviceSubType,
                pKDeviceType,
                PlatformMapper.map(platform),
                serverAccount,
                serverDevice,
                serverEvent
            )
        }
    }
}