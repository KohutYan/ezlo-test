package com.kohutyan.ezlo_test.data.network.models

import com.google.gson.annotations.SerializedName

data class DeviceResponse(
    @SerializedName("Firmware")
    val firmware: String,
    @SerializedName("InternalIP")
    val internalIP: String,
    @SerializedName("LastAliveReported")
    val lastAliveReported: String,
    @SerializedName("MacAddress")
    val macAddress: String,
    @SerializedName("PK_Account")
    val pKAccount: Int,
    @SerializedName("PK_Device")
    val pKDevice: Int,
    @SerializedName("PK_DeviceSubType")
    val pKDeviceSubType: Int,
    @SerializedName("PK_DeviceType")
    val pKDeviceType: Int,
    @SerializedName("Platform")
    val platform: String?,
    @SerializedName("Server_Account")
    val serverAccount: String,
    @SerializedName("Server_Device")
    val serverDevice: String,
    @SerializedName("Server_Event")
    val serverEvent: String
)