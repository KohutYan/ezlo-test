package com.kohutyan.ezlo_test.data.db.entities

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = DEVICE_TABLE)
class DeviceEntity(
    val deviceName: String,
    val firmware: String,
    val internalIP: String,
    val lastAliveReported: String,
    val macAddress: String,
    val pKAccount: Int,
    @PrimaryKey
    val pKDevice: Int,
    val pKDeviceSubType: Int,
    val pKDeviceType: Int,
    val platform: String?,
    val serverAccount: String,
    val serverDevice: String,
    val serverEvent: String
)

const val DEVICE_TABLE = "DEVICE"

