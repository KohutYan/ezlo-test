package com.kohutyan.ezlo_test.data.db

import androidx.room.Database
import androidx.room.RoomDatabase
import com.kohutyan.ezlo_test.data.db.dao.DeviceDao
import com.kohutyan.ezlo_test.data.db.entities.DeviceEntity

@Database(
    version = 1, entities = [
        DeviceEntity::class
    ]
)
abstract class AppDatabase : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao

    companion object {
        const val DB_NAME = "EZLO_DB"
    }
}