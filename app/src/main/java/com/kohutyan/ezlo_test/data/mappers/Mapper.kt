package com.kohutyan.ezlo_test.data.mappers

interface Mapper<T, R> where  T : Any?, R : Any? {
    fun map(input: T): R
}