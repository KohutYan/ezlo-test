package com.kohutyan.ezlo_test.data.network.service

import com.kohutyan.ezlo_test.data.network.models.DevicesResponse
import retrofit2.http.GET

interface DeviceService {
    @GET(GET_DEVICES)
    suspend fun getDevices(): DevicesResponse

    private companion object {
        const val GET_DEVICES = "/test_android/items.test"
    }
}