package com.kohutyan.ezlo_test.data.repo

import com.kohutyan.ezlo_test.data.db.dao.DeviceDao
import com.kohutyan.ezlo_test.data.network.service.DeviceService
import com.kohutyan.ezlo_test.domain.models.DeviceModel
import com.kohutyan.ezlo_test.domain.repo.IDeviceRepo
import com.kohutyan.ezlo_test.utils.mappers.DeviceEntityToModelMapper
import com.kohutyan.ezlo_test.utils.mappers.DeviceModelToEntityMapper
import com.kohutyan.ezlo_test.utils.mappers.DeviceResponseToModelMapper
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class DeviceRepo @Inject constructor(
    private val service: DeviceService, private val dao: DeviceDao
) : IDeviceRepo {
    override suspend fun getDevicesFromApi(): Flow<List<DeviceModel>> = flow {
        val devices =
            service.getDevices().devices.sortedBy { it.pKDevice }.mapIndexed { index, it ->
                DeviceResponseToModelMapper.map(it to index)
            }

        dao.clearTable()
        dao.insertAll(devices.map { DeviceModelToEntityMapper.map(it) })

        emit(devices)
    }

    override suspend fun watchDevicesFromDb(): Flow<List<DeviceModel>> {
        return dao.getAll().map { list -> list.map { DeviceEntityToModelMapper.map(it) } }
    }

    override suspend fun deleteDeviceById(id: Int) {
        dao.deleteById(id)
    }

    override suspend fun updateDevice(device: DeviceModel) {
        dao.updateDevice(DeviceModelToEntityMapper.map(device))
    }

}