package com.kohutyan.ezlo_test.data.network

import android.net.ParseException
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.kohutyan.ezlo_test.utils.DATE_FORMAT
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale

class DateDeserializer : JsonDeserializer<Date> {
    override fun deserialize(
        json: JsonElement,
        typeOfT: Type?,
        context: JsonDeserializationContext?
    ): Date? {
        val date: String = json.asString
        val format = SimpleDateFormat(DATE_FORMAT, Locale.getDefault())

        return try {
            format.parse(date)
        } catch (exp: ParseException) {
            null
        }
    }
}