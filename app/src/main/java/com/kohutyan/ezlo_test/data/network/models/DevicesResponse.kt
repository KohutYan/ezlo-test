package com.kohutyan.ezlo_test.data.network.models


import com.google.gson.annotations.SerializedName

data class DevicesResponse(
    @SerializedName("Devices")
    val devices: List<DeviceResponse>
)