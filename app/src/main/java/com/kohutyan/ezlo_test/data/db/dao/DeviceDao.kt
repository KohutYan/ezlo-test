package com.kohutyan.ezlo_test.data.db.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import androidx.room.Update
import com.kohutyan.ezlo_test.data.db.entities.DEVICE_TABLE
import com.kohutyan.ezlo_test.data.db.entities.DeviceEntity
import kotlinx.coroutines.flow.Flow

@Dao
interface DeviceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAll(devices: List<DeviceEntity>)

    @Query("SELECT * FROM $DEVICE_TABLE")
    fun getAll(): Flow<List<DeviceEntity>>

    @Query("DELETE FROM $DEVICE_TABLE")
    suspend fun clearTable()

    @Query("DELETE FROM $DEVICE_TABLE WHERE pKDevice = :id")
    suspend fun deleteById(id: Int)

    @Update
    suspend fun updateDevice(device: DeviceEntity)
}