package com.kohutyan.ezlo_test.presentation.features.item_list.models

import androidx.annotation.DrawableRes
import com.kohutyan.ezlo_test.R

enum class Platform(
    val platformKey: String?,
    @DrawableRes val imageRes: Int = R.drawable.vera_edge_big
) {
    SERCOMM_G450("Sercomm G450", R.drawable.vera_plus_big),
    SERCOMM_G550("Sercomm G550", R.drawable.vera_secure_big),
    MICASAVERDE_VERALITE("MiCasaVerde VeraLite"),
    SERCOMM_NA900("Sercomm NA900"),
    SERCOMM_NA301("Sercomm NA301"),
    SERCOMM_NA930("Sercomm NA930"),
    NO_KEY(null),
}