package com.kohutyan.ezlo_test.presentation.routing.types

import android.os.Bundle
import androidx.navigation.NavType
import com.google.gson.Gson
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device

class DeviceParamType : NavType<Device>(isNullableAllowed = false) {
    override fun get(bundle: Bundle, key: String): Device? {
        return bundle.getParcelable(key)
    }

    override fun parseValue(value: String): Device {
        return Gson().fromJson(value, Device::class.java)
    }

    override fun put(bundle: Bundle, key: String, value: Device) {
        bundle.putParcelable(key, value)
    }
}