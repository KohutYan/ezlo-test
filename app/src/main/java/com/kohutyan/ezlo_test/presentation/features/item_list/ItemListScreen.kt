package com.kohutyan.ezlo_test.presentation.features.item_list

import android.util.Log
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Icon
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavArgument
import androidx.navigation.NavHostController
import androidx.navigation.navArgument
import com.google.gson.Gson
import com.kohutyan.ezlo_test.R
import com.kohutyan.ezlo_test.core.theme.Purple40
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device
import com.kohutyan.ezlo_test.presentation.features.item_list.widgets.AnimatedCircularProgressIndicator
import com.kohutyan.ezlo_test.presentation.features.item_list.widgets.Header
import com.kohutyan.ezlo_test.presentation.features.item_list.widgets.ItemCard
import com.kohutyan.ezlo_test.utils.network.DataState

@Composable
fun ItemListScreen(navController: NavHostController) {
    val viewModel = hiltViewModel<ItemListViewModel>()

    val devices = remember { mutableStateOf((listOf<Device>())) }
    val openAlertDialog = remember { mutableStateOf(false) }
    val deletableItemId = remember { mutableStateOf<Int?>(null) }
    val updateEnabled = remember { mutableStateOf(true) }

    when (viewModel.updateState.value) {
        is DataState.Error -> {
            updateEnabled.value = true
        }

        DataState.Loading -> {
            updateEnabled.value = false
        }

        is DataState.Success -> {
            updateEnabled.value = true
        }
    }

    when (viewModel.devices.value) {
        is DataState.Success -> {
            devices.value = (viewModel.devices.value as DataState.Success<List<Device>>).data
        }

        is DataState.Loading -> {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                AnimatedCircularProgressIndicator()
            }
        }

        is DataState.Error -> {
            Column(
                modifier = Modifier.fillMaxSize(),
                horizontalAlignment = Alignment.CenterHorizontally,
                verticalArrangement = Arrangement.Center
            ) {
                (viewModel.devices.value as DataState.Error).exception.message?.let { Text(it) }
            }
            Log.e(
                "ItemListScreen",
                "ItemListScreen: ${(viewModel.devices.value as DataState.Error).exception}",
            )
        }
    }

    when {
        openAlertDialog.value -> {
            DeleteAlertDialog(
                onDismissRequest = {
                    openAlertDialog.value = false
                },
                onConfirmation = {
                    try {
                        viewModel.deleteDeviceFromDb(deletableItemId.value!!)

                    } catch (e: Exception) {
                        Log.e("ItemListScreen", "ItemListScreen: $e")
                    }
                    openAlertDialog.value = false
                },
                dialogTitle = stringResource(id = R.string.delete_dialog_title),
                dialogText = stringResource(id = R.string.delete_dialog_desc),
            )

        }
    }

    Column {
        Box(modifier = Modifier.fillMaxWidth()) {
            Header()
            UpdateButtonHeader(enabled = updateEnabled.value) {
                viewModel.getDevices()
            }
        }
        LazyColumn {
            items(devices.value.size) { index ->
                val item = devices.value[index]

                ItemCard(item = item, onClick = {
                    try {
                        val gson = Gson()

                        val json = gson.toJson(item)
                        navController.navigate("detail/${json}") {
                            navArgument("item", builder = {
                                NavArgument.Builder().setDefaultValue(item).build()
                            })
                        }

                    } catch (e: Exception) {
                        Log.e("ItemListScreen", "ItemListScreen: ${e.message}")
                        Log.e("ItemListScreen", "ItemListScreen: ${e.stackTrace}")
                    }
                }, onLongClick = {
                    openAlertDialog.value = true
                    deletableItemId.value = item.pKDevice
                })
            }
        }
    }
}

@Composable
fun UpdateButtonHeader(enabled: Boolean, onClick: () -> Unit) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(24.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.End
    ) {
        Icon(
            modifier = Modifier
                .size(32.dp)
                .clickable(enabled = enabled, onClick = onClick),
            painter = painterResource(id = R.drawable.baseline_refresh_24),
            contentDescription = null,
            tint = if (enabled) Purple40 else Purple40.copy(alpha = 0.5f)
        )
    }
}

@Composable
fun DeleteAlertDialog(
    onDismissRequest: () -> Unit,
    onConfirmation: () -> Unit,
    dialogTitle: String,
    dialogText: String,
) {
    AlertDialog(modifier = Modifier
        .padding(bottom = 24.dp)
        .fillMaxWidth(), title = {
        Text(text = dialogTitle)
    }, text = {
        Text(text = dialogText)
    }, onDismissRequest = {
        onDismissRequest()
    }, confirmButton = {
        TextButton(onClick = {
            onConfirmation()
        }) {
            Text(stringResource(id = R.string.ok))
        }
    }, dismissButton = {
        TextButton(onClick = {
            onDismissRequest()
        }) {
            Text(stringResource(id = R.string.cancel))
        }
    })
}
