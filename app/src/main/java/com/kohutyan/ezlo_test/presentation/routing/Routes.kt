package com.kohutyan.ezlo_test.presentation.routing

enum class Routes(val route: String, val args: List<String> = listOf()) {
    LIST(route = "list"),
    DETAIL(route = "detail/{item}", args = listOf("item"))
}