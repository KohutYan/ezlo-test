package com.kohutyan.ezlo_test.presentation.features.item_list

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.Spacer
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.foundation.layout.width
import androidx.compose.material3.Icon
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Text
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.kohutyan.ezlo_test.R
import com.kohutyan.ezlo_test.core.theme.Purple40
import com.kohutyan.ezlo_test.core.theme.PurpleGrey40
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device
import com.kohutyan.ezlo_test.presentation.features.item_list.widgets.Header

@Composable
fun ItemDetailScreen(item: Device) {
    val viewModel = hiltViewModel<ItemDetailsViewModel>()

    var nameText by rememberSaveable { mutableStateOf(item.deviceName) }
    val isInEditMode = remember { mutableStateOf(false) }

    Column(
        modifier = Modifier.fillMaxSize()
    ) {
        Box(modifier = Modifier.fillMaxWidth()) {
            Header()
            UpdateDeviceHeader(
                inEditMode = isInEditMode.value,
                isAcceptEnabled = nameText.isNotBlank(),
                onCloseClick = {
                    isInEditMode.value = false
                    nameText = item.deviceName
                },
                onEditClick = {
                    isInEditMode.value = true
                },
                onAcceptClick = {
                    viewModel.updateDeviceName(item, nameText)
                    isInEditMode.value = false
                })
        }
        Column(
            modifier = Modifier.padding(16.dp)
        ) {
            Row {
                Image(
                    painter = painterResource(item.platform.imageRes),
                    contentDescription = null,
                    modifier = Modifier.size(128.dp)
                )
                if (isInEditMode.value) {
                    TextField(
                        value = nameText,
                        textStyle = MaterialTheme.typography.headlineMedium,
                        onValueChange = {
                            nameText = it
                        },
                    )
                }
                Text(
                    text = if (nameText == item.deviceName) item.deviceName else nameText,
                    style = MaterialTheme.typography.headlineMedium
                )
            }
            Spacer(modifier = Modifier.height(16.dp))
            Text(
                text = stringResource(R.string.sn, item.pKDevice),
                style = MaterialTheme.typography.bodyMedium
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = stringResource(R.string.max_address, item.macAddress),
                style = MaterialTheme.typography.bodyMedium
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(
                text = stringResource(R.string.firmware, item.firmware),
                style = MaterialTheme.typography.bodyMedium
            )
            Spacer(modifier = Modifier.height(8.dp))
            Text(text = item.platform.platformKey?.let { stringResource(R.string.model, it) }
                ?: stringResource(R.string.no_key), style = MaterialTheme.typography.bodyMedium)
        }
    }
}


@Composable
fun UpdateDeviceHeader(
    inEditMode: Boolean,
    isAcceptEnabled: Boolean = true,
    onEditClick: () -> Unit,
    onCloseClick: () -> Unit,
    onAcceptClick: () -> Unit
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .padding(24.dp),
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.End
    ) {
        if (!inEditMode) Icon(
            modifier = Modifier
                .size(32.dp)
                .clickable(onClick = onEditClick),
            painter = painterResource(id = R.drawable.baseline_edit_24),
            contentDescription = null,
            tint = Purple40
        )
        else {
            Icon(
                modifier = Modifier
                    .size(32.dp)
                    .clickable(onClick = onCloseClick),
                painter = painterResource(id = R.drawable.baseline_close_24),
                contentDescription = null,
                tint = PurpleGrey40
            )
            Spacer(modifier = Modifier.width(8.dp))
            Icon(
                modifier = Modifier
                    .size(32.dp)
                    .clickable(enabled = isAcceptEnabled, onClick = onAcceptClick),
                painter = painterResource(id = R.drawable.baseline_check_24),
                contentDescription = null,
                tint = if (isAcceptEnabled) Purple40 else Purple40.copy(alpha = 0.5f)
            )
        }

    }
}