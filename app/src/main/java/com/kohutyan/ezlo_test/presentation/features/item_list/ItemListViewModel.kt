package com.kohutyan.ezlo_test.presentation.features.item_list

import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kohutyan.ezlo_test.domain.use_cases.DeleteDeviceById
import com.kohutyan.ezlo_test.domain.use_cases.GetDevicesFromApi
import com.kohutyan.ezlo_test.domain.use_cases.ObserveDevicesFromDb
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device
import com.kohutyan.ezlo_test.utils.mappers.DeviceModelToDevice
import com.kohutyan.ezlo_test.utils.network.DataState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.flow.onStart
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemListViewModel @Inject constructor(
    private val getDevicesFromApi: GetDevicesFromApi,
    private val observeDevicesFromDb: ObserveDevicesFromDb,
    private val deleteDeviceById: DeleteDeviceById,
) : ViewModel() {

    val devices: MutableState<DataState<List<Device>>> = mutableStateOf(DataState.Loading)
    val updateState: MutableState<DataState<Unit>> = mutableStateOf(DataState.Loading)

    init {
        getDevices()
        observeItemsFromDb()
    }

    fun getDevices() {
        viewModelScope.launch {
            getDevicesFromApi.run(Unit).onStart {
                updateState.value = DataState.Loading
            }.catch {
                updateState.value = DataState.Error(it as Exception)
            }.collect {
                updateState.value = DataState.Success(Unit)
            }
        }
    }

    fun deleteDeviceFromDb(id: Int) {
        viewModelScope.launch {
            deleteDeviceById.run(id)
        }
    }

    private fun observeItemsFromDb() {
        viewModelScope.launch {
            observeDevicesFromDb.run(Unit).map { list -> list.toList().sortedBy { it.pKDevice } }
                .map { list -> list.map { DeviceModelToDevice.map(it) } }.catch {
                    devices.value = DataState.Error(it as Exception)
                }.collect {
                    devices.value = DataState.Success(it)
                }

        }
    }

}