package com.kohutyan.ezlo_test.presentation

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.compose.ui.Modifier
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.kohutyan.ezlo_test.core.theme.EzloTheme
import com.kohutyan.ezlo_test.presentation.features.item_list.ItemDetailScreen
import com.kohutyan.ezlo_test.presentation.features.item_list.ItemListScreen
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device
import com.kohutyan.ezlo_test.presentation.routing.types.DeviceParamType
import com.kohutyan.ezlo_test.presentation.routing.Routes
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            EzloTheme {
                Surface(
                    modifier = Modifier.fillMaxSize(), color = MaterialTheme.colorScheme.background
                ) {
                    EzloApp()
                }
            }
        }
    }
}

@Composable
fun EzloApp() {
    val navController = rememberNavController()
    NavHost(navController = navController, startDestination = Routes.LIST.route) {
        composable(Routes.LIST.route) {
            ItemListScreen(navController)
        }
        composable(Routes.DETAIL.route, arguments = listOf(navArgument(Routes.DETAIL.args.first()) {
            type = DeviceParamType()
        })) { backStackEntry ->
            val item =
                remember { backStackEntry.arguments?.getParcelable<Device>(Routes.DETAIL.args.first()) }
            ItemDetailScreen(item!!)
        }
    }
}
