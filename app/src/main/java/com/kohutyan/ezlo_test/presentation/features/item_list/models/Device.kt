package com.kohutyan.ezlo_test.presentation.features.item_list.models

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class Device(
    val deviceName: String,
    val firmware: String,
    val internalIP: String,
    val lastAliveReported: String,
    val macAddress: String,
    val pKAccount: Int,
    val pKDevice: Int,
    val pKDeviceSubType: Int,
    val pKDeviceType: Int,
    val platform: Platform,
    val serverAccount: String,
    val serverDevice: String,
    val serverEvent: String
) : Parcelable