package com.kohutyan.ezlo_test.presentation.features.item_list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.kohutyan.ezlo_test.domain.use_cases.UpdateDevice
import com.kohutyan.ezlo_test.presentation.features.item_list.models.Device
import com.kohutyan.ezlo_test.utils.mappers.DeviceToDeviceModelMapper
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ItemDetailsViewModel @Inject constructor(
    private val updateDevice: UpdateDevice
) : ViewModel() {

    fun updateDeviceName(device: Device, name: String) {
        viewModelScope.launch {
            updateDevice.run(DeviceToDeviceModelMapper.map(device.copy(deviceName = name)))
        }
    }
}